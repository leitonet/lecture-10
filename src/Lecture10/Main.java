package Lecture10;


import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    static AtomicInteger counter = new AtomicInteger(1);

    public static void main(String[] args) {
        Thread myThreadClass = new MyThread();


        Thread myThreadInterface = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < 1_000_000; i++) {
                    increment();
                }
                System.out.println("MyThreadIntarface done: " + counter);
            }
        });

        myThreadInterface.start();
        myThreadClass.start();
    }


    static void increment(){
        counter.incrementAndGet();
    }

    public static class MyThread extends Thread{

        @Override
        public void run() {
            for(int i = 0; i < 1_000_000; i++) {
                increment();
            }
            System.out.println("MyThreadClass done: " + counter);
        }
    }
}
